<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'iot' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'O2YOw@bK*`y,EE06Y.@@*&6j=hK]6)5kh-x_Y8 ?h$:j.@U+o9w4x9^AiVUT$.FX' );
define( 'SECURE_AUTH_KEY',  'Ia8+JW;G;<zL]PId;Y(t%Vbhsz>pv`C1e@Fa^3.to[~K<DT:lFu CvQ/g2V-8:$1' );
define( 'LOGGED_IN_KEY',    '5MiL?4&_d>j-doYWd:bChqX,<2j]#cZ)g(vd=8`<g/U.6/9*t~/b@<2b$SB6R<x=' );
define( 'NONCE_KEY',        'x-.!J3@q96Qa++G.WDxXT`7iyiG0KJ[TD%Eh@1QCFgVu{nd?N{R6u?1I#8U Bbre' );
define( 'AUTH_SALT',        '4`px(7c6y*HuPSo913(-c-|y/x(ckRy4?| RO2;Gz^H71ZETD]!aF5wp+^$ci11|' );
define( 'SECURE_AUTH_SALT', 'L7#!dbW!Yuw@`!wj?r@LaV,&}[@k@rv$9*Q]CPL||:qzGThxH2]2b$T1U=INsa-?' );
define( 'LOGGED_IN_SALT',   'yq/b?hKb$7a+j/TCj3`M:b&dxt;(V>B3]Tddu<uM<Bg$ZwCDkbETo5DfkP.K:l:K' );
define( 'NONCE_SALT',       'rq?KTgR>FB&#):,cTePu5h/cW<@63P,.CUG,KK>lX@C ^fr#O3SZYKIC%VZW`J:g' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
