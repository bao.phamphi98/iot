<?php
/**
 * Blog Main File.
 *
 * @package APPWAY
 * @author  TemplatePath
 * @version 1.0
 */

get_header();
global $wp_query;
$data  = \APPWAY\Includes\Classes\Common::instance()->data( 'blog' )->get();
$class = ( $data->get( 'layout' ) != 'full' ) ? 'col-xs-12 col-sm-12 col-md-12 col-lg-9' : 'col-xs-12 col-sm-12 col-md-12';
do_action( 'appway_banner', $data );

if ( class_exists( '\Elementor\Plugin' ) AND $data->get( 'tpl-type' ) == 'e' AND $data->get( 'tpl-elementor' ) ) {
	echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $data->get( 'tpl-elementor' ) );
} else {
	?>


<!--Start blog area-->
 <section class="sidebar-page-container">
        <div class="container">
            <div class="row">
                <!--Sidebar Start-->
                <?php
				if ( $data->get( 'layout' ) == 'left' ) {
					do_action( 'appway_sidebar', $data );
				}
				?>               
            
             <div class="<?php echo esc_attr( $class ); ?> col-md-12 col-sm-12 content-side"> 
				 <div class="blog-content">
                        <?php
						while ( have_posts() ) :
							the_post();
							appway_template_load( 'templates/blog/blog.php', compact( 'data' ) );
						endwhile;
						
					
						?>
						
						<div class="post-pagination">
						<?php appway_the_pagination(); ?>
					</div>
                    </div>
                </div>
                <!--Sidebar Start-->
                <?php
				if ( $data->get( 'layout' ) == 'right' ) {
					do_action( 'appway_sidebar', $data );
				}
				?>
            </div>
        </div>
    </section> 
    <!--End blog area--> 
	<?php
}
get_footer();
