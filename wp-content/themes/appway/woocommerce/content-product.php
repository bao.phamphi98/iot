<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

global $wp_query;
$data  = \APPWAY\Includes\Classes\Common::instance()->data( 'archive' )->get();
$layout = $data->get( 'layout' );
$sidebar = $data->get( 'sidebar' );
$layout = ( $layout ) ? $layout : 'right';
$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
if (is_active_sidebar( $sidebar )) {$layout = 'right';} else{$layout = 'full';}

if( !$layout || $layout == 'full' ) $classes[] = 'col-md-4 product-block'; else $classes[] = 'col-md-4 product-block'; ?>

<div <?php post_class( $classes ); ?> >
	<div class="product-block-one">
        <div class="inner-box">
			<?php
            /**
             * Hook: woocommerce_before_shop_loop_item.
             *
             * @hooked woocommerce_template_loop_product_link_open - 10
             */
            do_action( 'woocommerce_before_shop_loop_item' );
        
            /**
             * Hook: woocommerce_before_shop_loop_item_title.
             *
             * @hooked woocommerce_show_product_loop_sale_flash - 10
             * @hooked woocommerce_template_loop_product_thumbnail - 10
             */
			 
			 $post_thumbnail_id = get_post_thumbnail_id($post->ID);
         	 $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			 ?>
             <figure class="image-box">
                <?php woocommerce_template_loop_product_thumbnail(); ?>
                <div class="right-option">
                    <div class="icon"><span class="flaticon-more"></span></div>
                    <ul class="content-box">
                        <li><a href="<?php echo esc_url( $post_thumbnail_url ); ?>" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-zoom-in"></i><span><?php esc_html_e( 'Quick View', 'appway' ); ?></span></a></li>
                        <li><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><i class="flaticon-center"></i><span><?php esc_html_e( 'Add to Whishlist', 'appway' ); ?></span></a></li>
                    </ul>
                </div>                                     
            </figure>
        	<div class="lower-content">
                <ul class="rating clearfix">
                    <li><?php woocommerce_template_loop_rating(); ?></li>
                </ul>
                <p><?php woocommerce_template_loop_price(); ?></p>
                <h3><a href="<?php echo esc_url(get_the_permalink(get_the_id())); ?>"><?php the_title(); ?></a></h3>
            </div>
            <?php
            /**
             * Hook: woocommerce_after_shop_loop_item.
             *
             * @hooked woocommerce_template_loop_product_link_close - 5
             * @hooked woocommerce_template_loop_add_to_cart - 10
             */
            do_action( 'woocommerce_after_shop_loop_item' );
            ?>
		</div>
	</div>
</div>