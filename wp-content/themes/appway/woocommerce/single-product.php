<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( 'shop' );
$data    = \APPWAY\Includes\Classes\Common::instance()->data( 'single' )->get();
$layout = $data->get( 'layout' );
$sidebar = $data->get( 'sidebar' );
$layout = ( $layout ) ? $layout : 'right';
$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
if (is_active_sidebar( $sidebar )) {$layout = 'right';} else{$layout = 'full';}
$class = ( !$layout || $layout == 'full' ) ? 'col-xl-12 col-lg-12 col-md-12 col-sm-12' : 'col-xs-12 col-sm-12 col-md-12 col-lg-8';
if ( class_exists( '\Elementor\Plugin' ) && $data->get( 'tpl-type' ) == 'e') {
	
	while(have_posts()) {
	   the_post();
	   the_content();
    }

} else {
?>

<?php if ( class_exists( '\Elementor\Plugin' )):?>
	<?php do_action( 'appway_banner', $data );?>
<?php else:?>
<!-- Page Title -->
<section class="page-title" style="background-image: url('<?php echo esc_url( $data->get( 'banner' ) ); ?>');">
    <div class="auto-container">
        <div class="content-box">
            <div class="content-wrapper">
                <div class="title">
                    <h1><?php echo wp_kses( $data->get( 'title' ), true ); ?></h1>
                </div>
                <ul class="bread-crumb clearfix">
                    <?php echo appway_the_breadcrumb(); ?>
                </ul>
            </div>                    
        </div>
    </div>
</section>
<!-- Page Title -->
<?php endif;?>
<!-- page-title -->
<?php if ( $data->get( 'banner' ) ) : ?>
<section class="page-title" style="background-image: url('<?php echo esc_url( $data->get( 'banner' ) ); ?>');">
<?php else : ?>	  
<section class="page-title" style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/assets/images/background/pagetitle-bg.png');?>);">
<?php endif ;?>	 

	<div class="anim-icons">
		<div class="icon icon-1"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-17.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-2 rotate-me"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-18.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-3 rotate-me"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-19.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-4"></div>
	</div>
	<div class="container">
		<div class="content-box clearfix">
			<div class="title-box pull-left">
				<h1><?php echo  wp_title(''); ?></h1>
			</div>
			<ul class="bread-crumb pull-right">
				<?php echo appway_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</section>
<!-- page-title end -->

<!--Start shop area-->
<section class="single-shop-area">
    <div class="auto-container">
    	<div class="row">
			
            <!-- sidebar area -->
			<?php
				if ( $data->get( 'layout' ) == 'left' ) {
					do_action( 'appway_sidebar', $data );
					
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					do_action( 'woocommerce_sidebar' );
				}
			?>
			
            <div class="<?php echo esc_attr($class);?>">
            	
                <div class="shop-content">
                	<?php
						/**
						 * woocommerce_before_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php wc_get_template_part( 'content', 'single-product' ); ?>
						<?php endwhile; // end of the loop. ?>
					<?php
						/**
						 * woocommerce_after_main_content hook
						 *
						 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
						 */
						do_action( 'woocommerce_after_main_content' );
					?>
                </div>
                
            </div>
		
        	<!-- sidebar area -->
			<?php
				if ( $data->get( 'layout' ) == 'right' ) {
					do_action( 'appway_sidebar', $data );
					
					/**
					 * woocommerce_sidebar hook
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					do_action( 'woocommerce_sidebar' );
				}
			?>
		</div>
	</div>
</section>
<?php
}
get_footer( 'shop' );
