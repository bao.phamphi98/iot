<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.6.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
global $wp_query;
$data  = \APPWAY\Includes\Classes\Common::instance()->data( 'single' )->get();
$layout = $data->get( 'layout' );
$sidebar = $data->get( 'sidebar' );
$layout = ( $layout ) ? $layout : 'right';
$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
if (is_active_sidebar( $sidebar )) {$layout = 'right';} else{$layout = 'full';}
$class = ( !$layout || $layout == 'full' ) ? 'col-xs-12 col-sm-12 col-md-12' : 'col-xs-12 col-sm-12 col-md-12 col-lg-8';
if ( class_exists( '\Elementor\Plugin' ) AND $data->get( 'tpl-type' ) == 'e' AND $data->get( 'tpl-elementor' ) ) {
	echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $data->get( 'tpl-elementor' ) );
} else {
?>


	  
<!-- page-title -->
<!-- page-title -->
<?php if ( $data->get( 'banner' ) ) : ?>
<section class="page-title" style="background-image: url('<?php echo esc_url( $data->get( 'banner' ) ); ?>');">
<?php else : ?>	  
<section class="page-title" style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/assets/images/background/pagetitle-bg.png');?>);">
<?php endif ;?>	 

	<div class="anim-icons">
		<div class="icon icon-1"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-17.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-2 rotate-me"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-18.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-3 rotate-me"><img src="<?php echo esc_url(get_template_directory_uri().'/assets/images/icons/anim-icon-19.png');?>" alt="<?php esc_attr_e('image','appway') ?>"></div>
		<div class="icon icon-4"></div>
	</div>
	<div class="container">
		<div class="content-box clearfix">
			<div class="title-box pull-left">
				<h1><?php echo wp_kses( $data->get( 'title' ), true ); ?></h1>
			</div>
			<ul class="bread-crumb pull-right">
				<?php echo appway_the_breadcrumb(); ?>
			</ul>
		</div>
	</div>
</section>
<!-- page-title end -->




<!--Start Shop area-->
 <section class="sidebar-page-container">
        <div class="container">
            <div class="row">
			
			<!-- sidebar area -->
          
			<!-- sidebar area -->
			
			 <div class="content-side col-md-12 col-sm-12">
            	<div class="our-shop ">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                	
                    <!--Sort By-->
                    <div class="items-sorting row m-0 justify-content-between">
                        <div class="text">
                            <?php woocommerce_result_count(); ?>
                        </div>
                        <form action="#">
                            <div class="form-group">
                                <?php
									/**
									 * woocommerce_before_shop_loop hook
									 *
									 * @hooked woocommerce_result_count - 20
									 * @hooked woocommerce_catalog_ordering - 30
									 */
									do_action( 'woocommerce_before_shop_loop' );
								?>
                            </div>
                        </form>
                    </div>
                    
                <?php endif; ?>
				<?php
                    /**
                     * woocommerce_before_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                     * @hooked woocommerce_breadcrumb - 20
                     */
                    do_action( 'woocommerce_before_main_content' );
                ?>
                
                <?php
                    /**
                     * woocommerce_archive_description hook
                     *
                     * @hooked woocommerce_taxonomy_archive_description - 10
                     * @hooked woocommerce_product_archive_description - 10
                     */
                    do_action( 'woocommerce_archive_description' );
                ?>
                
				<?php if ( have_posts() ) : ?>
            
					<?php woocommerce_product_loop_start(); ?>
        
                        <?php woocommerce_product_subcategories(); ?>
        
                        <?php while ( have_posts() ) : the_post(); ?>
        
                            <?php wc_get_template_part( 'content', 'product' ); ?>
        
                        <?php endwhile; // end of the loop. ?>
        
                    <?php woocommerce_product_loop_end(); ?>
        
                    <?php
                        /**
                         * woocommerce_after_shop_loop hook
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                    ?>
        
                <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
        
                    <?php wc_get_template( 'loop/no-products-found.php' ); ?>
        
                <?php endif; ?>
                
				<?php
                    /**
                     * woocommerce_after_main_content hook
                     *
                     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                     */
                    do_action( 'woocommerce_after_main_content' );
                ?>
	
				</div>
            </div>
            <!-- sidebar area -->
            
    		<!--Sidebar-->
    
		</div>
	</div>
</section>

<?php
}
get_footer( 'shop' );
