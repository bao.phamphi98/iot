<?php
/**
 * Blog Post Main File.
 *
 * @package APPWAY
 * @author  Template Path
 * @version 1.0
 */

get_header();
$data    = \APPWAY\Includes\Classes\Common::instance()->data( 'single' )->get();

$layout = ( $data->get( 'layout' ) ) ? $data->get('layout') : 'right';

$class   = ( $layout != 'full' ) ? 'col-sm-12 col-md-12 col-lg-9' : 'col-md-12';
$options = appway_WSH()->option();
do_action( 'appway_banner', $data );

if ( class_exists( '\Elementor\Plugin' ) && $data->get( 'tpl-type' ) == 'e') {
	
	while(have_posts()) {
	   the_post();
	   the_content();
    }

} else {
	?>

<!-- page-title end -->
    <section class="sidebar-page-container mrsingle">
        <div class="container">
            <div class="row">
					<?php
					if ( $data->get( 'layout' ) == 'left' ) {
						do_action( 'appway_sidebar', $data );
					}
					?>
		
				<div class="wp-style <?php echo esc_attr( $class ); ?> col-md-12 col-sm-12 content-side">	
					 <div class="blog-single-content">
						<?php
						while ( have_posts() ) :
							the_post();
							?>
							<div <?php post_class(); ?>>
								<?php appway_template_load( 'templates/blog-single/single-content.php', compact( 'options', 'data' ) ); ?>
							</div>
						<?php endwhile; ?>
						
					</div>
				</div>	
					<!-- page content -->
					<?php
					if ( $layout == 'right' ) {
						$data->set('sidebar', 'default-sidebar');
						do_action( 'appway_sidebar', $data );
					}
					?>
			
			</div>
		</div>
	</section><!-- content with sidebar -->
	<?php
}
get_footer();
