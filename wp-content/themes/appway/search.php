<?php
/**
 * Tag Main File.
 *
 * @package APPWAY
 * @author  TemplatePath
 * @version 1.0
 */

get_header();
global $wp_query;
$data  = \APPWAY\Includes\Classes\Common::instance()->data( 'search' )->get();
$class = ( $data->get( 'layout' ) != 'full' ) ? 'col-xs-12 col-sm-12 col-md-12 col-lg-9' : 'col-xs-12 col-sm-12 col-md-12';
do_action( 'appway_banner', $data );

if ( class_exists( '\Elementor\Plugin' ) AND $data->get( 'tpl-type' ) == 'e' AND $data->get( 'tpl-elementor' ) ) {
	echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $data->get( 'tpl-elementor' ) );
} else {
	?>

<?php if( have_posts() ) : ?> 
   <section class="sidebar-page-container">
        <div class="container">
            <div class="row">
                <!--Sidebar Start-->
                <?php
				if ( $data->get( 'layout' ) == 'left' ) {
					do_action( 'appway_sidebar', $data );
				}
				?>               
            
             <div class="<?php echo esc_attr( $class ); ?> col-md-12 col-sm-12 content-side"> 
				 <div class="blog-content">
                        <?php
						while ( have_posts() ) :
							the_post();
							appway_template_load( 'templates/blog/blog.php', compact( 'data' ) );
						endwhile;
						wp_reset_postdata();
						appway_the_pagination( $wp_query->max_num_pages );
						?>
                    </div>
                </div>
                <!--Sidebar Start-->
                <?php
				if ( $data->get( 'layout' ) == 'right' ) {
					do_action( 'appway_sidebar', $data );
				}
				?>
            </div>
        </div>
    </section> 
<?php else : ?>  
<?php get_template_part('templates/search'); ?>	
<?php endif; ?>
  <!--End blog area--> 
	<?php
}
get_footer();
