<?php
/**
 * Default Template Main File.
 *
 * @package APPWAY
 * @author  TemplatePath
 * @version 1.0
 */

get_header();
$data = \APPWAY\Includes\Classes\Common::instance()->data('single')->get();
$layout = $data->get('layout') ? $data->get('layout') : 'right';
$class = ($data->get('layout') != 'full') ? 'col-lg-12 col-sm-12' : 'col-xs-12 col-sm-12 col-md-12';
do_action('appway_banner', $data);
?>


 <section class="sidebar-page-container mrsingle" style="padding-top: 0;">
        <div class="container-fluid">
            <div class="row">
				<?php
if ($data->get('layout') == 'left') {
    do_action('appway_sidebar', $data);
}
?>
			<div class="wp-style <?php echo esc_attr($class); ?> col-md-12 col-sm-12 content-side">
				<div class="blog-single-content">
					<div class=" text clearfix"> <!-- class in div mr_page -->
					<?php while (have_posts()): the_post();?>
							<h2><?php //the_title(); ?></h2>
							<!-- <div class="container">
								<div class="row banner-custom crossbar">
									<div class="col-12">
										<h1 class="title-banner">
											Pet health Care Platform
										</h1>
										<span class="title-span">반려동물 건강측정기 맥스펫케어</span>
									</div>
									<div class="col-12 col-md-7 col-lg-7 row">
										<div class="col-10 mt-5 pt-2">
											<div class="message">
												<div class="d-flex">
													<div class="message-icon">
														<img src="wp-content/themes/appway/assets/images/icons/smile2.png" alt="">
													</div>
													<div class="message-content ml-2">
														강아지는 자기 자신을 사랑하는 것보다 지구 상에서 유일하게 당신을 더 사랑합니다.
														<br>
														<span class="d-flex justify-content-end mt-2 span-message">By. 조쉬빌링스</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-9 mt-5 pt-2 offset-3">
											<div class="message">
												<div class="d-flex">
													<div class="message-content ml-2">
															그래서 강아지는 당신의 인생에 최고의 선물인거죠!
														<br>
													</div>
													<div class="message-icon">
														<img src="wp-content/themes/appway/assets/images/icons/smile2.png" alt="">
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div> -->

							<?php the_content();?>

						<?php endwhile;?>
					<div class="clearfix"></div>
					<?php
$defaults = array(
    'before' => '<div class="paginate_links">' . esc_html__('Pages:', 'appway'),
    'after' => '</div>',

);
wp_link_pages($defaults);
?>
					</div>
					<?php comments_template()?>
				</div>
				</div>
				<?php
if ($layout == 'right') {
    $data->set('sidebar', 'default-sidebar');
    do_action('appway_sidebar', $data);
}
?>
		</div>
	</div>
</section><!-- blog section with pagination -->
<?php get_footer();?>
